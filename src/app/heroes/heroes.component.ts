import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivoService } from '../activo.service';
import { Persona } from '../persona';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  personas: Persona[];
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<Persona[]> = new Subject();

  constructor(private activoService: ActivoService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getPersonas();
  }

  getPersonas(): void {
    this.activoService.getPersonas().subscribe(personas => {
      this.personas = personas['content']; 
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    })
  }

}
