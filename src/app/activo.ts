export class Activo {
    id: number;
    created_at: string;
    updated_at: string;
    alto: string;
    ancho: string;
    color: string;
    descripcion: string;
    fecha_baja: string;
    fecha_compra: string;
    largo: string;
    nombre: string;
    numero_interno_inventario: string;
    peso: string;
    serial: string;
    valor_compra: string;
}
