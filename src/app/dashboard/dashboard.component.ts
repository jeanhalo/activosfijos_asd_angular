import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Activo } from '../activo';
import { ActivoService } from '../activo.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  activos: Activo[];
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<Activo[]> = new Subject();

  constructor(private activoService: ActivoService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getActivos();
  }

  getActivos(): void {
    this.activoService.getActivos().subscribe(activos => {
      this.activos = activos['content']; 
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    })
  }

}
