import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivoService } from '../activo.service';
import { Area } from '../area';

@Component({
  selector: 'app-dtprueba',
  templateUrl: './dtprueba.component.html',
  styleUrls: ['./dtprueba.component.css']
})
export class DtpruebaComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  areas: Area[];
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<Area[]> = new Subject();

  constructor(private activoService: ActivoService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getAreas();
  }

  getAreas(): void {
    this.activoService.getAreas().subscribe(areas => {
      this.areas = areas['content']; 
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    })
  }

}
