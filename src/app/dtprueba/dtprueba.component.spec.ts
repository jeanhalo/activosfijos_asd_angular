import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtpruebaComponent } from './dtprueba.component';

describe('DtpruebaComponent', () => {
  let component: DtpruebaComponent;
  let fixture: ComponentFixture<DtpruebaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtpruebaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtpruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
