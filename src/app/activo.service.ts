import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';
import { Observable, of } from 'rxjs';
import { Activo } from './activo';
import { catchError, map, tap } from 'rxjs/operators';
import { Persona } from './persona';
import { Area } from './area';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ActivoService {

  private activosUrl = 'http://localhost:8080/activosfijos';  // URL to web api
  private personasUrl = 'http://localhost:8080/personas';  // URL to web api
  private areasUrl = 'http://localhost:8080/areas';  // URL to web api

  constructor(
    private messageService: MessageService,
    private http: HttpClient
  ) { }

  /** GET activos from the server */
  getActivos(): Observable<Activo[]> {
    // TODO: enviar el mensaje _after_ buscando a los héroes
    return this.http.get<Activo[]>(this.activosUrl)
      .pipe(
        tap(_ => this.log('fetched activos')),
        catchError(this.handleError('getActivos', []))
      );
  }

  /** GET personas from the server */
  getPersonas(): Observable<Persona[]> {
    // TODO: enviar el mensaje _after_ buscando a los héroes
    return this.http.get<Persona[]>(this.personasUrl)
      .pipe(
        tap(_ => this.log('fetched personas')),
        catchError(this.handleError('getPersonas', []))
      );
  }

  /** GET areas from the server */
  getAreas(): Observable<Area[]> {
    // TODO: enviar el mensaje _after_ buscando a los héroes
    return this.http.get<Area[]>(this.areasUrl)
      .pipe(
        tap(_ => this.log('fetched areas')),
        catchError(this.handleError('getAreas', []))
      );
  }

  /** Log a ActivoService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`ActivoService: ${message}`);
  }

  /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
