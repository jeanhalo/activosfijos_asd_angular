import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DtpruebaComponent } from './dtprueba/dtprueba.component';

const routes: Routes = [
  { path: '', redirectTo: '/activosfijos', pathMatch: 'full'},
  { path: 'personas', component: HeroesComponent },
  { path: 'activosfijos', component: DashboardComponent },
  { path: 'areas', component: DtpruebaComponent}
]

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule { }
