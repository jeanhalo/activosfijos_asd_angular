export class Persona {
    id: number;
    createdAt: string;
    updatedAt: string;
    numeroIdentificacion: string;
    nombres: string;
    apellidos: string;
    telefonoCelular: string;
}
