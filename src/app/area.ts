export class Area {
    id: number;
    createdAt: string;
    updatedAt: string;
    nombreArea: string;
    direccion: string;
}
